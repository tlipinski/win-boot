#!/usr/bin/env bash

set -e

DEVICE=$1
WIN_ISO=$2

: ${DEVICE:?"Missing param"}
: ${WIN_ISO:?"Missing param"}

PARTITION=${DEVICE}1
BOOT_PEN_MNT=/mnt/boot-pen
WIN_ISO_MNT=/mnt/win-iso

if [[ $DEVICE == /dev/sd[a-b] ]]; then
  echo Don\'t do this to $DEVICE!
  exit 1
fi

unmount() {
  sudo umount $WIN_ISO_MNT
  sudo rm -rf $WIN_ISO_MNT

  sudo umount $BOOT_PEN_MNT
  sudo rm -rf $BOOT_PEN_MNT
}

trap "echo === Cleanup; unmount" EXIT

echo === Unmounting
set +e
sudo umount $PARTITION
unmount
set -e

echo === Preparing partition
sudo parted -s $DEVICE mklabel msdos
sudo parted -s $DEVICE mkpart primary ntfs 0% 100%
sudo parted -s $DEVICE set 1 boot on

echo === Formatting partition
sudo mkfs.ntfs -f $PARTITION

echo === Writing boot sector
sudo ms-sys -7 $DEVICE

echo === Mounting pendrive
sudo mkdir -p $BOOT_PEN_MNT
sudo mount $PARTITION $BOOT_PEN_MNT

echo === Mounting iso
sudo mkdir -p $WIN_ISO_MNT
sudo mount -o loop "$WIN_ISO" $WIN_ISO_MNT

echo === Copying
sudo rsync -a -h --info=progress2 $WIN_ISO_MNT/* $BOOT_PEN_MNT

echo === Syncing
sync

